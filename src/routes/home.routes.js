import { FormValidations } from '../app/middlewares'
import AuthValidations from '../app/validations/auth.validations'
import Router from '../libraries/controller/router'

Router.get('/', [], 'HomeController@welcome')

export default Router.export()
