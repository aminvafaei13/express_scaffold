import HomeRoutes from './home.routes'
import AuthRoutes from './auth.routes'
import UserRoutes from './users.routes'

export default app => {
  app.use('/', HomeRoutes)
  app.use('/auth', AuthRoutes)
  app.use('/users', UserRoutes)
}
