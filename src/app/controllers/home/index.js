import Controller from '../../../libraries/controller'

export default class HomeController extends Controller {
  async welcome (req, res, next) {
    this.sendResponse(res, '', 'Welcome to Express!', 200)
  }
}
